/**
 * @author MASSIEU--ROCABOIS Malo INFO1D1
 */

/**
 * Imports needed for the class
 */
import mail.MailItem;

/**
 * TestMailItem class
 */
public class TestMailItem {

	/**
	 * Main entry point 
	 * 
	 * @param args The command line arguments
	 */
	public static void main(String[] args) {

		new TestMailItem();

	}
	
	/**
	 * Class constructor
	 */
	private TestMailItem() {

		this.constructorTest();

		this.printTest();

	}

	/**
	 * Tests the constructor
	 */
	private void constructorTest() {

		System.out.println("\n*** Constructor test + getFrom() / getTo() / getMessage() ***");

		System.out.println("\nNormal cases :\n");

		MailItem item1 = new MailItem("Briac", "Jean", "Six P'Hack");

		constructorTestCase(item1, "Briac", "Jean", "Six P'Hack");

		MailItem item2 = new MailItem("Jean", "", "Oui chef!");

		constructorTestCase(item2, "Jean", "", "Oui chef!");

		System.out.println("\nEdge cases :\n");

		MailItem item3 = new MailItem(null, "Louis", "Flop");

		constructorTestCase(item3, "sender", "Louis", "Flop");

		MailItem item4 = new MailItem("Lenny", null, "Flop");

		constructorTestCase(item4, "Lenny", "receiver", "Flop");

		MailItem item5 = new MailItem("Lenny", "Louis", null);

		constructorTestCase(item5, "Lenny", "Louis", "");

		MailItem item6 = new MailItem(null, null, null);

		constructorTestCase(item6, "sender", "receiver", "");

	}

	/**
	 * Tests calls to the constructor 
	 * 
	 * @param item The mail item to test
	 * @param expFrom The expected sender
	 * @param expTo The expected receiver
	 * @param expMessage The expected message
	 */
	private void constructorTestCase(MailItem item, String expFrom, String expTo, String expMessage) {

		System.out.print("Expected from: " + expFrom + " - Actual from: " + item.getFrom());

		if (item.getFrom().equals(expFrom))
			System.out.println(" - OK");
		else
			System.out.println(" - ERROR");

		System.out.print("Expected to: " + expTo + " - Actual to: " + item.getTo());

		if (item.getTo().equals(expTo))
			System.out.println(" - OK");
		else
			System.out.println(" - ERROR");

		System.out.print("Expected message: " + expMessage + " - Actual message: " + item.getMessage());

		if (item.getMessage().equals(expMessage))
			System.out.println(" - OK\n");
		else
			System.out.println(" - ERROR\n");

	}

	/**
	 * Tests the print method
	 */
	private void printTest() {

		System.out.println("\n*** Print test ***");

		System.out.println("\nNormal cases :\n");

		MailItem item1 = new MailItem("Briac", "Jean", "Votez Six P'Hack");

		item1.print();

		MailItem item2 = new MailItem("Jean", "", "Oui chef!");

		item2.print();

		System.out.println("\nEdge cases :\n");

		MailItem item3 = new MailItem(null, "Louis", "Flop");

		item3.print();

		MailItem item4 = new MailItem("Lenny", null, "Flop");

		item4.print();

		MailItem item5 = new MailItem("Lenny", "Louis", null);

		item5.print();

		MailItem item6 = new MailItem(null, null, null);

		item6.print();

	}
	
}

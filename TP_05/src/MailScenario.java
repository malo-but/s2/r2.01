/**
 * @author MASSIEU--ROCABOIS Malo INFO1D1
 */

/**
 * The package containing the mail class
 */
import mail.*;

/**
 * The package containing the ArrayList class
 */
import java.util.ArrayList;

/**
 * The package containing the Iterator class
 */
import java.util.Iterator;

/**
 * MailScenario class
 */
public class MailScenario {

	/**
	 * Main entry point
	 * 
	 * @param args The command line arguments
	 */
	public static void main(String[] args) {

		new MailScenario();

	}

	/**
	 * Class constructor
	 */
	public MailScenario() {

		ArrayList<String> spams = new ArrayList<String>();

		spams.add("mean");

		spams.add("T'Hack OS");

		spams.add("ratio");

		spams.add("pessi");

		MailServer server = new MailServer(spams);

		ArrayList<MailItem> mails = new ArrayList<MailItem>();

		mails.add(new MailItem("John", "Paul", "Hello Pessi!"));

		mails.add(new MailItem("Paul", "John", "Hi"));

		mails.add(new MailItem("John", "Paul", "How are you ? RATIO"));

		mails.add(new MailItem("Paul", "John", "I'm fine, thanks. You are mean"));

		mails.add(new MailItem("Paul", "John", "Let's vote for T'Hack OS"));

		mails.add(new MailItem("Paul", "John", "Ok good bye !"));

		this.postTest(server, mails);

		this.howManyMailItemTest(server, "John");
		
		this.getNextMailItemTest(server, "John");
		
		this.howManyMailItemTest(server, "Paul");

		this.getNextMailItemTest(server, "Paul");

		// server.displayOldMails();

	}

	/**
	 * Post test
	 * 
	 * @param server The mail server
	 * @param mails The mails to post
	 */
	private void postTest(MailServer server, ArrayList<MailItem> mails) {
	
		Iterator<MailItem> it = mails.iterator();

		while (it.hasNext()) {

			server.post(it.next());

		}

	}

	/**
	 * How many mail item test
	 * 
	 * @param server The mail server
	 * @param user The user's name
	 */
	private void howManyMailItemTest(MailServer server, String user) {

		System.out.println(user + " received " + server.howManyMailItem(user) + " mails :\n");

	}

	/**
	 * Get next mail item test
	 * 
	 * @param server The mail server
	 * @param user The user's name
	 */
	private void getNextMailItemTest(MailServer server, String user) {

		while (server.howManyMailItem(user) > 0) {

			server.getNextMailItem(user).print();

		}

	}
	
}

/**
 * @author MASSIEU--ROCABOIS Malo INFO1D1
 */

/**
 * The package containing the class
 */
package mail;

import java.util.ArrayList;

/**
 * MailClient class
 */
public class MailClient {

	/**
	 * The client's name
	 */
	private String user;

	/**
	 * The mail server
	 */
	private MailServer server;

	/**
	 * Class constructor
	 * 
	 * @param server The mail server
	 * @param user The user's name
	 */
	public MailClient(MailServer server, String user) {

		if (user != null)
			this.user = user;
		else 
			this.user = "defaultUser";

		if (server != null)
			this.server = server;
		else 
			this.server = new MailServer(new ArrayList<String>());
			
	}

	/**
	 * Returns the last mail received by the user
	 * 
	 * @return The last mail received by the user
	 */
	public MailItem getNextMailItem() {

		return this.server.getNextMailItem(user);

	}

	/**
	 * Prints the last mail received by the user
	 */
	public void printNextmailItem() {

		MailItem temp = this.server.getNextMailItem(this.user);

		if (temp != null)
			temp.print();
		else
			System.out.println("No mail");

	}

	/**
	 * Sends a mail
	 * 
	 * @param to The receiver
	 * @param message The message
	 */
	public void sendMailItem(String to, String message) {

		if (to == null)
			to = "defaultReceiver";
		
		if (message == null)
			message = "";

		MailItem mail = new MailItem(this.user, to, message);

		server.post(mail);

	}

}

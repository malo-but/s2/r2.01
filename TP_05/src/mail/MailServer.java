/**
 * @author MASSIEU--ROCABOIS Malo INFO1D1
 */

/**
 * The package containing the class
 */
package mail;

/**
 * Package needed for ArrayList
 */
import java.util.ArrayList;

/**
 * Package needed for iterators
 */
import java.util.Iterator;

/**
 * MailServer class
 */
public class MailServer {

	/**
	 * The list of mails
	 */
	private ArrayList<MailItem> items;

	/**
	 * The anti-spam filter
	 */
	private AntiSpam antiSpam;

	/**
	 * The list of spams
	 */
	private ArrayList<MailItem> spams;

	/**
	 * The list of spammers
	 */
	private ArrayList<String> spammers;

	/**
	 * The list of old mails
	 */
	private ArrayList<MailItem> oldMails;
	
	/**
	 * Class constructor
	 */
	public MailServer(ArrayList<String> filters) {

		this.items = new ArrayList<MailItem>();

		if (filters != null)
			this.antiSpam = new AntiSpam(filters);
		else 
			this.antiSpam = new AntiSpam(new ArrayList<String>());

		this.spams = new ArrayList<MailItem>();

		this.spammers = new ArrayList<String>();

		this.oldMails = new ArrayList<MailItem>();

	}

	/**
	 * Returns the number of mail received by the given user
	 * 
	 * @param who The user
	 * @return The numberof mail received
	 */
	public int howManyMailItem(String who) {

		int count = 0;

		Iterator<MailItem> it = this.items.iterator();

		while (it.hasNext()) {

			if (it.next().getTo().equals(who)) {

				count++;

			}

		}

		return count;

	}

	/**
	 * Returns the next mail destined to the given user
	 * 
	 * @param who The user
	 * @return The user's last mail
	 */
	public MailItem getNextMailItem(String who) {

		MailItem item = null;

		Iterator<MailItem> it = this.items.iterator();

		boolean found = false;

		while (it.hasNext() && !found) {

			MailItem currentItem = it.next();

			if (currentItem.getTo().equals(who)) {

				found = true;

				item = currentItem;

				this.items.remove(currentItem);

				this.oldMails.add(currentItem);

			}

		}

		return item;

	}

	/**
	 * Adds a new message to the server
	 * 
	 * @param item The new message
	 */
	public void post(MailItem item) {

		if (item != null) {

			String newMessage = item.getMessage();

			if (this.spammers.contains(item.getFrom())) {
				
				newMessage = "[SPAMMER WARNING] " + newMessage;
				
			}

			if (this.antiSpam.scan(item.getMessage())){
				
				this.spammers.add(item.getFrom());

				newMessage = "[SPAM] " + newMessage;
								
			}

			MailItem newItem = new MailItem(item.getFrom(), item.getTo(), newMessage);

			this.items.add(newItem);

			if (newItem.getMessage().contains("[SPAM]")) {

				this.spams.add(newItem);

			}
			
		} else {

			System.out.println("Object is null, nothing was changed.");

		}

	}

	/**
	 * Displays the list of deleted mails sorted by sender
	 */
	public void displayOldMails() {

		ArrayList<String> users = new ArrayList<String>();

		for (int i = 0; i < this.oldMails.size(); i++) {

			String user = this.oldMails.get(i).getFrom();

			if (!users.contains(user)) {

				users.add(user);

				System.out.println("Mails sent by " + user + ":");

				for (int j = 0; j < this.oldMails.size(); j++) {

					if (this.oldMails.get(j).getFrom().equals(user)) {

						this.oldMails.get(j).print();

					}

				}

			}

		}

	}

}

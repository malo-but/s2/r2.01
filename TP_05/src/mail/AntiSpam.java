/**
 * @author MASSIEU--ROCABOIS Malo INFO1D1
 */

/**
 * The package containing the class
 */
package mail;

/**
 * Package needed for ArrayList
 */
import java.util.ArrayList;

/**
 * Package needed for iterators
 */
import java.util.Iterator;

/**
 * AntiSpam class
 */
public class AntiSpam {
	
	/**
	 * The list of filters
	 */
	private ArrayList<String> filters;

	/**
	 * Class constructor
	 * 
	 * @param filters The list of filters
	 */
	public AntiSpam(ArrayList<String> filters) {

		if (filters != null) {

			ArrayList<String> filtersCopy = new ArrayList<String>();

			for (String f : filters) {

				if (f != null) {

					filtersCopy.add(f);

				}

			}
			
			this.filters = filtersCopy;

		} else
			this.filters = new ArrayList<String>();

	}

	/**
	 * Adds a new wotd to the list of filters
	 * 
	 * @param f The word to add
	 */
	public void add(String f) {

		if (f != null) {

			if (!this.filters.contains(f)) 
				this.filters.add(f);

		}

	}

	/**
	 * Check if a message contains a word from the list of filters
	 * 
	 * @param message The message to check
	 * @return True if the message contains a word from the list of filters, false otherwise
	 */
	public boolean scan(String message) {

		boolean result = false;

		Iterator<String> it = this.filters.iterator();

		while (it.hasNext() && !result) {

			if (message.toUpperCase().contains(it.next().toUpperCase())) {

				result = true;

			}
	
		}

		return result;

	}

}

/**
 * @author MASSIEU--ROCABOIS Malo INFO1D1
 */

/**
 * The package containing the class
 */
package mail;

/**
 * MailItem class
 */
public class MailItem {

	/**
	 * Message sender
	 */
	private String from;

	/**
	 * Message receiver
	 */
	private String to;

	/**
	 * The content of the message
	 */
	private String message;

	/**
	 * Class constructor
	 * 
	 * @param from The sender
	 * @param to The receiver
	 * @param message The message
	 */
	public MailItem(String from, String to, String message) {

		if (from != null)
			this.from = from;
		else 
			this.from = "sender";

		if (to != null)
			this.to = to;
		else 
			this.to = "receiver";

		if (message != null)
			this.message = message;
		else 
			this.message = "";

	}

	/**
	 * Returns the name of the sender
	 * 
	 * @return The sender's name
	 */
	public String getFrom() {

		return this.from;

	}

	/**
	 * Returns the name of the receiver
	 * 
	 * @return The receiver's name
	 */
	public String getTo() {

		return this.to;

	}

	/**
	 * Returns the message
	 * 
	 * @return The message
	 */
	public String getMessage() {

		return this.message;

	}

	/**
	 * Prints the message 
	 */
	public void print() {

		System.out.println("From : " + from + "\nTo : " + to + "\n\"" + message + "\"\n");

	}	
	
}

/**
 * @author MASSIEU--ROCABOIS Malo INFO1D1
 */

/**
 * The package containing the class
 */
package mail;

/**
 * Package needed for ArrayList
 */
import java.util.ArrayList;

/**
 * Package needed for iterators
 */
import java.util.Iterator;

/**
 * MailServer class
 */
public class MailServer {

	/**
	 * The list of mails
	 */
	private ArrayList<MailItem> items;
	
	/**
	 * Class constructor
	 */
	public MailServer() {

		items = new ArrayList<MailItem>();

	}

	/**
	 * Returns the number of mail received by the given user
	 * 
	 * @param who The user
	 * @return The numberof mail received
	 */
	public int howManyMailItem(String who) {

		int count = 0;

		Iterator<MailItem> it = this.items.iterator();

		while (it.hasNext()) {

			if (it.next().getTo().equals(who)) {

				count++;

			}

		}

		return count;

	}

	/**
	 * Returns the next mail destined to the given user
	 * 
	 * @param who The user
	 * @return The user's last mail
	 */
	public MailItem getNextMailItem(String who) {

		MailItem item = null;

		Iterator<MailItem> it = this.items.iterator();

		boolean found = false;

		while (it.hasNext() && !found) {

			MailItem currentItem = it.next();

			if (currentItem.getTo().equals(who)) {

				found = true;

				item = currentItem;

				this.items.remove(currentItem);

			}

		}

		return item;

	}

	/**
	 * Adds a new message to the server
	 * 
	 * @param item The new message
	 */
	public void post(MailItem item) {

		if (item != null)
			this.items.add(item);
		else 
			System.out.println("Object is null, nothing was changed.");

	}

}

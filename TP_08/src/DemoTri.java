import pays.Pays;
import tri.*;

/**
 * DemoTri class
 */
public class DemoTri {
	
	/**
	 * Main	method
	 * 
	 * @param args The command line arguments
	 */
	public static void main(String[] args) {

		Pays[] tabPays = new Pays[10];
		tabPays [0] = new Pays ("Cuba" , 11423952, 110860);
		tabPays [1] = new Pays ("Chile" , 16454143, 756950);
		tabPays [2] = new Pays ("Russia" , 140702094, 17075200);
		tabPays [3] = new Pays ("Norway" , 4644457, 323802);
		tabPays [4] = new Pays ("Nigeria" , 138283240, 923768);
		tabPays [5] = new Pays ("Paraguay" , 6831306, 406750);
		tabPays [6] = new Pays ("Oman" , 3311640, 212460);
		tabPays [7] = new Pays ("Yemen" , 23013376, 406750);
		tabPays [8] = new Pays ("Togo" , 5858673, 56785);
		tabPays [9] = new Pays ("France" , 64057790 , 643427);

		TriParSelection triPays = new TriParSelection(tabPays);

		triPays.trier();

		System.out.println("Tri par sélection: ");

		for (int i = 1; i < tabPays.length; i++) {

			if (tabPays[i].compareTo(tabPays[i - 1]) > 0)
				System.out.println(tabPays[i].getNom() + " a une plus grande surface que " + tabPays[i - 1].getNom());
			else if (tabPays[i].compareTo(tabPays[i - 1]) < 0) {

				System.out.println(tabPays[i].getNom() + " a une plus petite surface que " + tabPays[i - 1].getNom());

				System.out.println("\n*** ERREUR: Le tableau n'est pas trié ***\n");

			}
			else
				System.out.println(tabPays[i].getNom() + " a la même surface que " + tabPays[i - 1].getNom());

		}

	}

}

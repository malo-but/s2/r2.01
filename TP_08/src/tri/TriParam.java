package tri;

/**
 * Class which represents a sorting algorithm
 */
public class TriParam<T extends Comparable<T>> implements ITri {

	/**
	 * Array to sort
	 */
	private T[] tab;

	/**
	 * Default constructor
	 *
	 * @param tab Array to sort
	 */
	public TriParam(T[] tab) {

		/* if (tab != null)
			this.tab = tab;
		else {
			
			// this.tab = new T[0];

			System.out.println("NULL value for array, value set to empty array");
			
		} */

		try {

			this.tab = tab;

			//int length = tab.length;

		} catch (NullPointerException e) {

			System.out.println("NULL value for array, value set to empty array");

		}

	}

	/**
	 * Sorts the array
	 */
	public void trier() {

		for (int i = 0; i < tab.length - 1; i++) {

			int minPos = minimumPosition(i);

			swap(i, minPos);

		}

	}

	/**
	 * Returns the position of the minimum value in the array
	 * 
	 * @param start Position from which the search starts
	 * @return Position of the minimum value
	 */
	private int minimumPosition(int start) {

		int minPos = start;

		for (int i = start + 1; i < tab.length; i++) {

			if (tab[i].compareTo(tab[minPos]) == -1)
				minPos = i;

		}

		return minPos;

	}

	/**
	 * Swaps two values in the array
	 * 
	 * @param i Position of the first value
	 * @param j Position of the second value
	 */
	private void swap(int i, int j) {

		T temp = tab[i];

		tab[i] = tab[j];

		tab[j] = temp;

	}

}
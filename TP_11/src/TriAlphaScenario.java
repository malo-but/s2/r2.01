import pays.*;
import java.util.ArrayList;

/**
 * Class testing the alphabetical sort
 */
public class TriAlphaScenario {

	/**
	 * Main entry point
	 * @param args The command line arguments
	 */
	public static void main(String[] args) {
		new TriAlphaScenario();
	}

	/**
	 * Constructor which launches the tests
	 */
	public TriAlphaScenario() {

		Population pop = new Population("../data/worldpop.txt", "../data/worldarea.txt");

		ArrayList<Pays> listePaysAlpha = pop.getListePays();

		int i = 1;
		boolean ok = true;

		while (ok && i < listePaysAlpha.size()) {

			if (listePaysAlpha.get(i-1).getNom().compareTo(listePaysAlpha.get(i).getNom()) > 0) {
				ok = false;
			}

			System.out.println(listePaysAlpha.get(i - 1).getNom());

			i++;
		}

		if (ok) {
			System.out.println("\n => Liste triée");
		} else {
			System.out.println("\n => Liste non triée");
		}

	}
	
}

import pays.*;
import utilitaire.*;

import java.util.ArrayList;

/**
 * Test class
 */
public class ScenarioDensity {

	/**
	 * Main entry point
	 * @param args The command line arguments
	 */
	public static void main(String[] args) {
		new ScenarioDensity();
	}

	/**
	 * Constructor
	 */
	public ScenarioDensity() {

		Population pop = new Population("../data/worldpop.txt", "../data/worldarea.txt");

		RWFile.writeMap(pop.computeDensity(), "../data/worldDensity.txt");

		ArrayList<String> densities = RWFile.readFile("../data/worldDensity.txt");

		for (String density : densities) {
			System.out.println(density);
		}

	}

}
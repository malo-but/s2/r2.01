import java.util.ArrayList;

/**
 * Class used to sort an array using the selection sort algorithm
 */
public class TriParSelectionG <T extends Comparable<T>> implements tri.ITri {

	/**
	 * Array to sort
	 */
	private ArrayList<T> tab;

	/**
	 * Default constructor
	 *
	 * @param tab Array to sort
	 */
	public TriParSelectionG(ArrayList<T> tab) {

		if (tab != null)
			this.tab = tab;
		else {
			
			this.tab = new ArrayList<T>();

			System.out.println("NULL value for array, value set to empty array");

		}

	}

	/**
	 * Sorts the array
	 */
	public void trier() {

		for (int i = 0; i < this.tab.size() - 1; i++) {

			int minPos = this.minimumPosition(i);

			this.swap(i, minPos);

		}

	}

	/**
	 * Returns the position of the minimum value in the array
	 * 
	 * @param start Position from which the search starts
	 * @return Position of the minimum value
	 */
	private int minimumPosition(int start) {

		int minPos = start;

		for (int i = start + 1; i < this.tab.size(); i++) {

			if (this.tab.get(i).compareTo(this.tab.get(minPos)) < 0)
				minPos = i;

		}

		return minPos;

	}

	/**
	 * Swaps two values in the array
	 * 
	 * @param i Position of the first value
	 * @param j Position of the second value
	 */
	private void swap(int i, int j) {

		T temp = this.tab.get(i);

		this.tab.set(i, this.tab.get(j));

		this.tab.set(j, temp);

	}

}
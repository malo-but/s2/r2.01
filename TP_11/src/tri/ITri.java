package tri;

/**
 * Interface which represents a sorting algorithm
 */
public interface ITri {

	/**
	 * Sorts an array
	 */
	public void trier();

}
import pays.*;

/**
 * classe de scenario de Population
 */
public class ScenarioPopulation {
  
  /**
   * Main entry point which launches the different tests
   * @param args Command line arguments
   */
  public static void main (String[] args) {
          
    // creation d'un objet Population    
    Population pop= new Population ("../data/worldpop.txt", "../data/worldarea.txt");
   
  
    System.out.println("Nombre de données population " + pop.getPopMap().size());
    System.out.println("Nombre de données surface " + pop.getAreaMap().size());
    for (Pays p :  pop.getListePays())
        System.out.println(p + "\n");
       
  }

}

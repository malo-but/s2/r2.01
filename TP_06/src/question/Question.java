package question;

/**
 * This class represents a question.
 */
public class Question {

	/**
	 * The question text.
	 */
	private String text;

	/**
	 * The answer to the question.
	 */
	private String answer;

	/**
	 * Constructs a question with empty question and answer.
	 */
	public Question() {

		this.text = "";

		this.answer = "";

	}

	/**
	 * Constructs a question with given question and answer.
	 * @param text the question text
	 * @param answer the answer
	 */
	public Question(String text, String answer) {

		if (text != null)

			this.text = text;

		else

			this.text = "";


		if (answer != null)
		
			this.answer = answer;

		else

			this.answer = "";

	}

	/**
	 * Gets the question text.
	 * @return the question text
	 */
	public String getText() {

		return this.text;

	}

	/**
	 * Gets the answer.
	 * @return the answer
	 */
	public String getAnswer() {

		return this.answer;

	}

	/**
	 * Sets the question text.
	 * @param text the question text
	 */
	public void setText(String text) {

		if (text != null)

			this.text = text;

		else

			System.out.println("Invalid text");

	}

	/**
	 * Sets the answer.
	 * @param answer the answer
	 */
	public void setAnswer(String answer) {
	

		if (answer != null)

			this.answer = answer;

		else

			System.out.println("Invalid answer");

	}

	/**
	 * Checks a given response for correctness.
	 * @param response the response to check
	 * @return true if the response was correct, false otherwise
	 */
	public boolean checkAnswer(String response) {

		boolean res = false;

		if (response != null)

			res = this.answer.equalsIgnoreCase(response);

		return res;

	}

	/**
	 * Displays this question.
	 */
	public void display() {

		System.out.println(this.text);

	}
	
}

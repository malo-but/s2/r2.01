package question;

import java.util.ArrayList;

/**
 * This class represents a quiz.
 */
public class Quiz {

	/**
	 * The list of questions.
	 */
	private ArrayList<Question> questionList;

	/**
	 * Constructs a quiz with no questions.
	 */
	public Quiz() {

		this.questionList = new ArrayList<Question>();

	}

	/**
	 * Constructs a quiz with the given questions.
	 * @param questions the list of questions
	 */
	public Quiz(ArrayList<Question> questions) {

		if (questions != null)

			this.questionList = questions;
		
		else

			this.questionList = new ArrayList<Question>();

	}

	/**
	 * Returns a question at a random index.
	 * @return a question at a random index
	 */
	public Question pickAtRandom() {

		return this.questionList.get((int)(Math.random() * this.questionList.size()));

	}

	/**
	 * Returns the question text at the given index.
	 * @param index the index of the question to return
	 * @return the question text at the given index
	 */
	public String getQuestionText(int index) {

		String res = null;

		if (index > 0 && index < this.questionList.size())

			res = this.questionList.get(index).getText();

		return res;

	}

	/**
	 * Returns the number of questions in the quiz.
	 * @return the number of questions in the quiz
	 */
	public int getQNumber() {

		return this.questionList.size();

	}

	/**
	 * Adds a question to the quiz.
	 * @param q the question to add
	 */
	public void add(Question q) {

		if (q != null)

			this.questionList.add(q);

	}

	/**
	 * Displays all the questions in the quiz.
	 */
	public void display() {

		for (Question q : this.questionList)

			System.out.println(q.getText());


	}

}

package question;

import java.util.ArrayList;

/**
 * This class represents a choice question.
 */
public class ChoiceQuestion extends Question {

	/**
	 * The list of choices.
	 */
	private ArrayList<String> choices;

	/**
	 * Constructs a choice question with no choices.
	 */
	public ChoiceQuestion() {

		super();

		this.choices = new ArrayList<String>();

	}

	/**
	 * Adds an answer choice to this question.
	 * @param choice the choice to add 
	 * @param correct true if this is the correct choice, false otherwise
	 */
	public void addChoice(String choice, boolean correct) {

		if (choice != null) {

			this.choices.add(choice);

		}

		if (correct) {

			this.setAnswer(Integer.toString(this.choices.size()));

		}

	}

	/**
	 * Displays this question.
	 */
	public void display() {

		super.display();

		for (int i = 0; i < this.choices.size(); i++) {

			System.out.println((i + 1) + ": " + this.choices.get(i));

		}

	}

}
import question.*;

import java.util.Scanner;

/**
 * This class is used to test the Question and ChoiceQuestion classes.
 */
public class ScenarioQuestion {
	
	/**
	 * This method is used to test the Question and ChoiceQuestion classes.
	 * @param args The command line arguments.
	 */
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		Question question = new Question("Who was the inventor of Java?", "James Gosling");

		question.display();
		
		String ans = input.nextLine();
		
		System.out.println(question.checkAnswer(ans));

		System.out.println("\n");

		ChoiceQuestion choiceQuestion = new ChoiceQuestion();

		choiceQuestion.setText("In which city is the Eiffel Tower?");

		choiceQuestion.addChoice("Paris", true);

		choiceQuestion.addChoice("Lyon", false);

		choiceQuestion.addChoice("Marseille", false);

		choiceQuestion.addChoice("Toulouse", false);

		choiceQuestion.display();

		System.out.println("Your answer: ");

		ans = input.nextLine();

		System.out.println(choiceQuestion.checkAnswer(ans));

		System.out.println("\n");

		ChoiceQuestion choiceQuestion2 = new ChoiceQuestion();

		choiceQuestion2.setText("Which one is not a programming language?");

		choiceQuestion2.addChoice("C", false);

		choiceQuestion2.addChoice("C++", false);
		
		choiceQuestion2.addChoice("HTML", true);
		
		choiceQuestion2.addChoice("Java", false);

		choiceQuestion2.display();

		System.out.println("Your answer: ");

		ans = input.nextLine();

		System.out.println(choiceQuestion2.checkAnswer(ans));

		input.close();
		
	}

}

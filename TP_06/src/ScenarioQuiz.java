import question.*;

import java.util.Scanner;

/**
 * This class is used to test the Quiz class.
 */
public class ScenarioQuiz {

	/**
	 * This method is used to test the Quiz class.
	 * @param args The command line arguments.
	 */
	public static void main(String[] args) {

		Quiz quiz = new Quiz();

		quiz.add(new Question("Who was the inventor of Java?", "James Gosling"));

		ChoiceQuestion choiceQuestion = new ChoiceQuestion();

		choiceQuestion.setText("In which city is the Eiffel Tower?");

		choiceQuestion.addChoice("Paris", true);

		choiceQuestion.addChoice("Lyon", false);

		choiceQuestion.addChoice("Marseille", false);

		choiceQuestion.addChoice("Toulouse", false);

		quiz.add(choiceQuestion);

		ChoiceQuestion choiceQuestion2 = new ChoiceQuestion();

		choiceQuestion2.setText("Which one is not a programming language?");

		choiceQuestion2.addChoice("C", false);

		choiceQuestion2.addChoice("C++", false);

		choiceQuestion2.addChoice("HTML", true);

		choiceQuestion2.addChoice("Java", false);

		quiz.add(choiceQuestion2);

		quiz.add(new Question("What is the capital of France?", "Paris"));

		quiz.add(new Question("What is the capital of Italy?", "Rome"));

		ChoiceQuestion choiceQuestion3 = new ChoiceQuestion();

		choiceQuestion3.setText("Which animal is the fastest?");

		choiceQuestion3.addChoice("Lion", false);

		choiceQuestion3.addChoice("Cheetah", true);

		choiceQuestion3.addChoice("Turtle", false);

		choiceQuestion3.addChoice("Elephant", false);

		quiz.add(choiceQuestion3);

		Scanner input = new Scanner(System.in);

		for (int i = 0; i < 10; i++) {

			Question question = quiz.pickAtRandom();

			question.display();

			System.out.println("Your answer: ");

			String answer = input.nextLine();

			System.out.println(question.checkAnswer(answer));

			System.out.println("\n");
			
		}

		input.close();

	}
	
}

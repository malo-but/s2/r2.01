import java.util.Arrays;

/**
 * Classe de test de la classe Etudiant
 */
public class ScenarioEtudiant {

	/**
	 * Méthode principale
	 * @param args Arguments de la ligne de commande
	 */
	public static void main(String[] args) {

		new ScenarioEtudiant();

	}

	/**
	 * Constructeur
	 */
	public ScenarioEtudiant() {

		this.envoiMessages(5);

		this.constructorTest();

		this.setNomTest();

		this.getNbMatieresTest();

		this.getUneNoteTest();

		this.moyenneMatiereTest();

		this.moyenneGeneraleTest();

		this.meilleureNoteTest();

		this.toStringTest();

	}

	/**
	 * Test un envoi de message dans la classe Etudiant
	 * @param nbEtud Nombre d'étudiants
	 */
	private void envoiMessages(int nbEtud) {

		String[] matieres = {"Maths", "Dev", "Web"};

		double[] coeff = {1, 4, 2};

		Etudiant[] etudiants = new Etudiant[nbEtud];

		double[] moyennesEtud = new double[nbEtud];

		int bestIndex = 0;

		for (int i = 0; i < nbEtud; i++) {

			etudiants[i] = new Etudiant(("Etudiant" + i), matieres, coeff, 3);

			moyennesEtud[i] = etudiants[i].moyenneGenerale();

			if (moyennesEtud[i] > moyennesEtud[bestIndex]) {
	
				bestIndex = i;
	
			}

		}

		System.out.println(etudiants[bestIndex].getNom() + " a la meilleure moyenne générale. / " + moyennesEtud[bestIndex]);

	}

	/**
	 * Test du constructeur
	 */
	private void constructorTest() {

		System.out.println("\n*** Test du constructeur ***\n");

		System.out.println("\t- Cas Normaux :");

		String[] matieres = {"Maths", "Dev", "Web"};

		double[] coeff = {1, 4, 2};

		Etudiant test1 = new Etudiant("Malo", matieres, coeff, 3);

		this.constructorTestCase(test1, "Malo", 3, coeff, 3);

		System.out.println("\t- Cas Limites :");

		System.out.print("\t\t");

		Etudiant test2 = new Etudiant("Malo", null, coeff, 0);

		this.constructorTestCase(test2, "Malo", 0, coeff, 1);

		System.out.print("\t\t");

		Etudiant test3 = new Etudiant("Malo", matieres, null, 0);

		this.constructorTestCase(test3, "Malo", 0, coeff, 1);

		System.out.print("\t\t");

		Etudiant test4 = new Etudiant("Malo", null, null, 0);

		this.constructorTestCase(test4, "Malo", 0, coeff, 1);

	}

	/**
	 * Test du constructeur
	 * @param stud Etudiant à tester
	 * @param nom Nom de l'étudiant
	 * @param nbMatieres Nombre de matières
	 * @param coeff Tableau de coefficients
	 * @param nbNotes Nombre de notes
	 */
	private void constructorTestCase(Etudiant stud, String nom, int nbMatieres, double[] coeff, int nbNotes) {

		boolean error = false;

		System.out.print("\t\tEtudiant(" + nom + "," + nbMatieres + "," + Arrays.toString(coeff) + "," + nbNotes + ") = ");

		if (stud.getNom() != nom) {

			System.out.println("Erreur : Nom invalide");

			error = true;

		}
		
		if (stud.getNbMatieres() != nbMatieres) {

			System.out.println("Erreur : Nombre de matières invalide");

			error = true;

		}

		if (!error) {

			System.out.println("OK");

		}

	}

	/**
	 * Test de setNom
	 */
	private void setNomTest() {

		System.out.println("\n*** Test de setNom ***\n");

		System.out.println("\t- Cas Normaux :");

		String[] matieres = {"Maths", "Dev", "Web"};

		double[] coeff = {1, 4, 2};

		Etudiant test1 = new Etudiant("Malo", matieres, coeff, 3);

		this.setNomTestCase(test1, "Malo", "Malo");

		System.out.println("\t- Cas Limites :");

		this.setNomTestCase(test1, " ", " ");

		this.setNomTestCase(test1, null, "");

	}

	/**
	 * Test de setNom
	 * @param stud Etudiant à tester
	 * @param nom Nom de l'étudiant
	 * @param expected Nom attendu
	 */
	private void setNomTestCase(Etudiant stud, String nom, String expected) {

		System.out.print("\t\tsetNom(" + nom + ") = ");

		stud.setNom(nom);

		if (stud.getNom() != expected) {

			System.out.println("\t\tErreur : Nom invalide");

		} else {

			System.out.println("OK");

		}

	}

	/**
	 * Test de getNbMatieres
	 */
	private void getNbMatieresTest() {

		System.out.println("\n*** Test de getNbMatieres ***\n");

		System.out.println("\t- Cas Normaux :");

		String[] matieres = {"Maths", "Dev", "Web"};

		double[] coeff = {1, 4, 2};

		Etudiant test1 = new Etudiant("Malo", matieres, coeff, 3);

		this.getNbMatieresTestCase(test1, 3);

		System.out.println("\t- Cas Limites :");

		System.out.print("\t\t");

		Etudiant test2 = new Etudiant("Malo", null, coeff, 3);

		this.getNbMatieresTestCase(test2, 0);

	}

	/**
	 * Test de getNbMatieres
	 * @param stud Etudiant à tester
	 * @param expected Nombre de matières attendu
	 */
	private void getNbMatieresTestCase(Etudiant stud, int expected) {

		System.out.print("\t\tgetNbMatieres() = ");

		if (stud.getNbMatieres() != expected) {

			System.out.println("Erreur : Nombre de matières invalide");

		} else {

			System.out.println("OK");

		}

	}

	/**
	 * Test de getUneNote
	 */
	private void getUneNoteTest() {

		System.out.println("\n*** Test de getUneNote ***\n");

		System.out.println("\t- Cas Normaux :");

		String[] matieres = {"Maths", "Dev", "Web"};

		double[] coeff = {1, 4, 2};

		Etudiant test1 = new Etudiant("Malo", matieres, coeff, 5);

		System.out.println("\t\t  Différentes notes aléatoires devrait être affichées :");

		for (int i = 0; i < 5; i++) {

			System.out.println("\t\t    " + test1.getUneNote(0, i));

		}

	}

	/**
	 * Test de getUneMatiere
	 */
	private void moyenneMatiereTest() {

		System.out.println("\n*** Test de moyenneMatiere ***\n");

		System.out.println("\t- Cas Normaux :");

		String[] matieres = {"Maths", "Dev", "Web"};

		double[] coeff = {1, 4, 2};

		Etudiant test1 = new Etudiant("Malo", matieres, coeff, 5);

		System.out.println("\t\t  Différentes moyennes aléatoires devrait être affichées :");

		for (int i = 0; i < matieres.length; i++) {

			System.out.println("\t\t    " + test1.moyenneMatiere(i));

		}

	}

	/**
	 * Test de moyenneGenerale
	 */
	private void moyenneGeneraleTest() {

		System.out.println("\n*** Test de moyenneGenerale ***\n");

		System.out.println("\t- Cas Normaux :");

		String[] matieres = {"Maths", "Dev", "Web"};

		double[] coeff = {1, 4, 2};

		Etudiant test1 = new Etudiant("Malo", matieres, coeff, 5);

		System.out.println("\t\t  Différentes moyennes aléatoires devrait être affichées :");

		for (int i = 0; i < 5; i++) {

			System.out.println("\t\t    " + test1.moyenneGenerale());

			test1 = new Etudiant("Malo", matieres, coeff, 5);

		}

	}

	/**
	 * Test de meilleureNote
	 */
	private void meilleureNoteTest() {

		System.out.println("\n*** Test de meilleureNote ***\n");

		System.out.println("\t- Cas Normaux :");

		String[] matieres = {"Maths", "Dev", "Web"};

		double[] coeff = {1, 4, 2};

		Etudiant test1 = new Etudiant("Malo", matieres, coeff, 5);

		System.out.println("\t\t  Différentes meilleures notes aléatoires devrait être affichées :");

		for (int i = 0; i < 5; i++) {

			System.out.println("\t\t    " + test1.meilleureNote());

			test1 = new Etudiant("Malo", matieres, coeff, 5);

		}

	}

	/**
	 * Test de toString
	 */
	private void toStringTest() {

		System.out.println("\n*** Test de toString ***\n");

		System.out.println("\t- Cas Normaux :");

		String[] matieres = {"Maths", "Dev", "Web"};

		double[] coeff = {1, 4, 2};

		Etudiant test1 = new Etudiant("Malo", matieres, coeff, 5);

		System.out.println("\t\t  Différents bulletins devrait être affichés :");

		for (int i = 0; i < 5; i++) {

			System.out.println(test1);

			test1 = new Etudiant("Malo", matieres, coeff, 5);

		}


	}
	
}

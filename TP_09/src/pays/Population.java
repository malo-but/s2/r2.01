package pays;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class which represents a population
 */
public class Population {
	
	/**
	 * List of countries
	 */
	private ArrayList<Pays> listePays;

	/**
	 * Map of population
	 */
	private HashMap<String, Double> popMap;

	/**
	 * Map of area
	 */
	private HashMap<String, Double> areaMap;

	/**
	 * Default constructor
	 * 
	 * @param popArray List of population
	 * @param areaArray List of area
	 */
	public Population(ArrayList<String> popArray, ArrayList<String> areaArray) {

		if (popArray != null)
			this.initializePopMap(popArray);
		else
			System.out.println("NULL value for population array");

		if (areaArray != null)
			this.initializeAreaMap(areaArray);
		else
			System.out.println("NULL value for area array");

		this.initializeListePays();

	}

	/**
	 * Getter for popMap
	 * @return popMap
	 */
	public HashMap<String, Double> getPopMap() {
		return this.popMap;
	}

	/**
	 * Getter for areaMap
	 * @return areaMap
	 */
	public HashMap<String, Double> getAreaMap() {
		return this.areaMap;
	}

	/**
	 * Getter for listePays
	 * @return listePays
	 */
	public ArrayList<Pays> getListePays() {
		return this.listePays;
	}

	/**
	 * Initialize popMap
	 * @param popArray List of population
	 */
	private void initializePopMap(ArrayList<String> popArray) {
		this.popMap = this.asMap(popArray);
	}

	/**
	 * Initialize areaMap
	 * @param areaArray List of area
	 */
	private void initializeAreaMap(ArrayList<String> areaArray) {
		this.areaMap = this.asMap(areaArray);
	}

	/**
	 * Initialize listePays
	 */
	private void initializeListePays() {

		this.listePays = new ArrayList<Pays>();
		
		if (this.popMap.size() == this.areaMap.size()) {

			for (int i = 0; i < this.popMap.keySet().size(); i++) {

				for (int j = 0; j < this.areaMap.keySet().size(); j++) {

					if (this.popMap.keySet().toArray()[i].equals(this.areaMap.keySet().toArray()[j])) {

						String country = this.popMap.keySet().toArray()[i].toString();

						this.listePays.add(new Pays(country, this.popMap.get(country), this.areaMap.get(country)));

					}

				}

			}

		}

	}

	/**
	 * Convert an ArrayList to a HashMap
	 * @param liste List to convert
	 * @return HashMap
	 */
	private HashMap<String, Double> asMap(ArrayList<String> liste) {

		HashMap<String, Double> map = new HashMap<String, Double>();

		if (liste != null) {

			for (String line : liste) {

				String country = this.extractCountry(line);
				double value = this.extractValue(line);

				map.put(country, value);

			}

		} else
			System.out.println("NULL parameter not allowed");

		return map;
	}

	/**
	 * Extract the country from a line
	 * @param line Line to extract from
	 * @return Country
	 */
	private String extractCountry(String line) {

		String country = "";

		if (line != null) {

			int i = 0;

			while (i < line.length() && !Character.isDigit(line.charAt(i)))
				i++;

			country = line.substring(0, i).trim();

		} else
			System.out.println("NULL value not allowed");

		return country;

	}

	/**
	 * Extract the value from a line
	 * @param line Line to extract from
	 * @return Value
	 */
	private double extractValue(String line) {

		double value = 0;

		if (line != null) {

			int i = 0;

			while (i < line.length() && !Character.isDigit(line.charAt(i)))
				i++;

			if (i != line.length()) {

				String valueString = line.substring(i).trim();
	
				value = Double.parseDouble(valueString);

			} else
				System.out.println("No digit found");

		} else
			System.out.println("NULL value not allowed");

		return value;

	}

}

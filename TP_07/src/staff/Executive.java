package staff;

/**
 * Class representing an executive employee
 */
public class Executive extends Employee {

	/**
	 * Bonus of the executive employee
	 */
	private double bonus;

	/**
	 * Constructor
	 * @param name the name of the staff member
	 * @param address the adress of the staff member
	 * @param phone the phone number of the staff member
	 * @param socialSecurityNumber the social security number of the employee
	 * @param payRate the pay rate of the employee
	 * @param bonus the bonus of the executive employee
	 */
	public Executive(String name, String address, String phone, String socialSecurityNumber, double payRate, double bonus) {

		super(name, address, phone, socialSecurityNumber, payRate);

		if (bonus > 0)

			this.bonus = bonus;

		else

			this.bonus = 0;

	}

	/**
	 * Adds a bonus to the executive employee
	 * @param bonus the bonus to add
	 */
	public void awardBonus(double bonus) {

		if (bonus > 0)

			this.bonus += bonus;

	}

	/**
	 * Pays the executive employee
	 */
	public double pay() {

		this.bonus = 0;	

		return this.payRate + this.bonus;

	}
	
}

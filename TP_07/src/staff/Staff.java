package staff;

import java.util.ArrayList;

/**
 * Class representing a staff team
 */
public class Staff {

	/**
	 * Staff list
	 */
	private ArrayList<StaffMember> staffList;

	/**
	 * Constructor
	 */
	public Staff() {

		this.staffList = new ArrayList<StaffMember>();

	}

	/**
	 * Add a new member to the staff
	 * @param member the new member
	 */
	public void addNewMember(StaffMember member) {

		if (member != null)

			this.staffList.add(member);

	}

	/**
	 * Get a member of the staff
	 * @param index the index of the member
	 * @return the member
	 */
	public StaffMember getMember(int index) {

		if (index >= 0 && index < this.staffList.size())

			return this.staffList.get(index);

		else

			return null;

	}

	/**
	 * Pays and displays the staff
	 */
	public void payday() {

		for (StaffMember staff : this.staffList) {

			System.out.println(staff);

			double salary = staff.pay();

			if (salary == 0)

				System.out.println("Thanks!");

			else 

				System.out.println("Paid : $" + salary);

			System.out.println("-----------------------------------");

		}

	}
	
}

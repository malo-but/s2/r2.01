package staff;

/**
 * Class representing a volunteer
 */
public class Volunteer extends StaffMember {

	/**
	 * Constructor
	 * @param name the name of the staff member
	 * @param address the adress of the staff member
	 * @param phone the phone number of the staff member
	 */
	public Volunteer(String name, String address, String phone) {

		super(name, address, phone);

	}

	/**
	 * Pays the staff member
	 */
	public double pay() {

		return 0;

	}
	
}

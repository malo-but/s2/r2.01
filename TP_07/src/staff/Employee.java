package staff;

/**
 * Class representing an employee
 */
public class Employee extends StaffMember {

	/**
	 * Social security number of the employee
	 */
	protected String socialSecurityNumber;

	/**
	 * Pay rate of the employee
	 */
	protected double payRate;

	/**
	 * Constructor
	 * @param name the name of the staff member
	 * @param address the adress of the staff member
	 * @param phone the phone number of the staff member
	 * @param socialSecurityNumber the social security number of the employee
	 * @param payRate the pay rate of the employee
	 */
	public Employee(String name, String address, String phone, String socialSecurityNumber, double payRate) {

		super(name, address, phone);

		if (socialSecurityNumber != null)

			this.socialSecurityNumber = socialSecurityNumber;

		else 

			this.socialSecurityNumber = "Default";

		if (payRate > 0)

			this.payRate = payRate;

		else

			this.payRate = 0;

	}

	/**
	 * Returns a string representation of the employee
	 */
	public String toString() {

		return super.toString() + "\nSocial Security Number: " + this.socialSecurityNumber + "\nPay Rate: " + this.payRate;

	}

	/**
	 * Pays the employee
	 */
	public double pay() {

		return this.payRate;

	}
	
}

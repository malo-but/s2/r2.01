package staff;

/**
 * Abstract class representing a staff member
 */
public abstract class StaffMember {

	/**
	 * Name of the staff member
	 */
	protected String name;

	/**
	 * Adress of the staff member
	 */
	protected String address;

	/**
	 * Phone number of the staff member
	 */
	protected String phone;

	/**
	 * Pays the staff member
	 * @return the amount of money paid
	 */
	public abstract double pay();

	/**
	 * Constructor
	 * @param name the name of the staff member
	 * @param address the adress of the staff member
	 * @param phone the phone number of the staff member
	 */
	public StaffMember(String name, String address, String phone) {
		
		if (name != null)

			this.name = name;

		else 

			this.name = "Default";

		if (address != null)

			this.address = address;

		else 
		
			this.address = "Default";

		if (phone != null)

			this.phone = phone;

		else

			this.phone = "00 00 00 00 00";

	}

	/**
	 * Returns a string representation of the staff member
	 */
	public String toString() {

		return "Name: " + name + "\nAdress: " + address + "\nPhone: " + phone;

	}
	
}

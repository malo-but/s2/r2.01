package staff;

/**
 * Class representing an hourly employee
 */
public class Hourly extends Employee {

	/**
	 * Hours worked by the employee
	 */
	private int hoursWorked;

	/**
	 * Constructor
	 * @param name the name of the staff member
	 * @param address the adress of the staff member
	 * @param phone the phone number of the staff member
	 * @param socialSecurityNumber the social security number of the employee
	 * @param payRate the pay rate of the employee
	 */
	public Hourly(String name, String address, String phone, String socialSecurityNumber, double payRate) {

		super(name, address, phone, socialSecurityNumber, payRate);

		this.hoursWorked = 0;

	}

	/**
	 * Adds hours to the employee
	 * @param hours the hours to add
	 */
	public void addHours(int hours) {

		if (hours > 0)

			this.hoursWorked += hours;

	}

	/**
	 * Pays the employee and resets the hours worked
	 */
	public double pay() {

		double payment = this.payRate * this.hoursWorked;

		this.hoursWorked = 0;

		return payment;

	}

	/**
	 * Returns a string representation of the employee
	 */
	public String toString() {

		return super.toString() + "\nHours Worked: " + this.hoursWorked;

	}

}

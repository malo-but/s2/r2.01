import staff.*;

/**
 * Test class for the staff package
 */
public class StaffScenario {
	
	/**
	 * Main method
	 */
	public static void main(String[] args) {

		Volunteer volunteer = new Volunteer("John", "1 rue de la paix", "01 02 03 04 05");

		Volunteer volunteer2 = new Volunteer("Karl", "28 rue de Normandie", "06 07 08 09 10");

		Employee employee = new Employee("Josie", "1 rue de la guerre", "11 12 13 14 15", "123456789", 1000);

		Employee employee2 = new Employee("Jack", "15 rue du Général", "16 17 18 19 20", "987654321", 1200);

		Executive executive = new Executive("Jean", "20 rue Maréchal Joffre", "21 22 23 24 25", "454654674656", 2000, 1000);

		executive.awardBonus(150);

		Hourly hourly = new Hourly("Jeanne", "1 rue de la liberté", "26 27 28 29 30", "123456789", 10);


		hourly.addHours(40);

		Staff staff = new Staff();

		staff.addNewMember(volunteer);

		staff.addNewMember(volunteer2);

		staff.addNewMember(employee);

		staff.addNewMember(employee2);

		staff.addNewMember(executive);

		staff.addNewMember(hourly);

		staff.payday();

	}

}

import utilitaire.*;
import java.util.ArrayList;

/**
 * Class used to test the RWFile class
 */
public class ScenarioRW {

	/**
	 * Main entry point 
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		new ScenarioRW();
	}

	/**
	 * Constructor which launches the tests
	 */
	public ScenarioRW() {

		System.out.println("*** worldarea.txt ***\n");
		ArrayList<String> area = RWFile.readFile("../data/worldarea.txt");
		for (String line : area) {
			System.out.println(line);
		}
		
		System.out.println("\n*** worldpop.txt ***\n");
		ArrayList<String> population = RWFile.readFile("../data/worldpop.txt");
		for (String line : population) {
			System.out.println(line);
		}

		area.addAll(population);
		RWFile.writeFile(area, "../data/area-pop.txt");

	}

}
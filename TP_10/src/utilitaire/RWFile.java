package utilitaire;

import java.util.ArrayList;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Utilitary class used to read and write in files
 */
public class RWFile {

	/**
	 * Reads a file and returns its content in an array list with each element representing one line.
	 * @param fileName The targeted file name
	 * @return The file's content
	 */
	public static ArrayList<String> readFile(String fileName) {

		ArrayList<String> res = new ArrayList<String>();

		if (fileName != null) {

			try {

				Scanner input = new Scanner(new FileReader(fileName));

				while (input.hasNextLine()) {
					res.add(input.nextLine());
				}

			} catch (FileNotFoundException e) {
				System.out.println("RWFile.readFile(String) => \"" + fileName + "\"file not found.");
			}

		} else 
			System.out.println("RWFile.readFile(String) => NULL parameter not allowed.");

		return res;

	}

	/**
	 * Writes the content of an array list in a file.
	 * @param liste The content we want to write 
	 * @param fileName The targeted file name
	 */
	public static void writeFile(ArrayList<String> liste, String fileName) {

		if (liste != null && fileName != null) {

			try {
	
				PrintWriter output = new PrintWriter(fileName);
	
				for (String line : liste) {
					output.println(line);
				}
	
				output.close();

			} catch (FileNotFoundException e) {
				System.out.println("RWFile.writeFile(ArrayList<String>, String) => \"" + fileName + "\"file not found.");
			}

		} else 
			System.out.println("RWFile.writeFile(ArrayList<String>, String) => NULL parameter not allowed.");

	}

}
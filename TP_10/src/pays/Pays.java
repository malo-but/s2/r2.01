package pays;

/**
 * Class which represents a country
 */
public class Pays implements Comparable<Pays> {
	/**
	 * Name of the country
	 */
	private String nom;

	/**
	 * Surface of the country
	 */
	private double surface;

	/**
	 * Population of the country
	 */
	private double population;

	/**
	 * Default constructor
	 */
	public Pays() {

		this.nom = "Default";
		this.surface = 0;
		this.population = 0;

	}

	/**
	 * Default constructor
	 *
	 * @param nom Name of the country
	 * @param population Population of the country
	 * @param surface Surface of the country
	 */
	public Pays(String nom, double population, double surface) {

		if (nom != null)
			this.nom = nom;
		else {

			this.nom = "Default";

			System.out.println("NULL value for name, value set to \"Default\"");

		}

		if (surface >= 0)
			this.surface = surface;
		else {
			this.surface = 0;

			System.out.println("Negative value for surface, value set to 0");

		}
		
		if (population >= 0)
			this.population = population;
		else {

			this.population = 0;

			System.out.println("Negative value for population, value set to 0");

		}

	}

	/**
	 * Compare the surface of the country with the surface of another country
	 * @param pays The other country
	 */
	public int compareTo(Pays pays) {

		int res = 0;

		if (this.surface < pays.getSurface())
			res = -1;
		else if (this.surface > pays.getSurface())
			res = 1;

		return res;

	}

	/**
	 * Return the name of the country
	 * @return The name of the country
	 */
	public String getNom() {

		return this.nom;

	}

	/**
	 * Return the surface of the country
	 * @return The surface of the country
	 */
	public double getSurface() {

		return this.surface;

	}

	/**
	 * Return the population of the country
	 * @return The population of the country
	 */
	public double getPopulation() {

		return this.population;

	}

	/**
	 * Set the name of the country
	 * @param nom The name of the country
	 */
	public void setNom(String nom) {

		if (nom != null)
			this.nom = nom;
		else
			System.out.println("NULL value for name, value not changed");

	}

	/**
	 * Set the surface of the country
	 * @param surface The surface of the country
	 */
	public void setSurface(double surface) {

		if (surface > 0)
			this.surface = surface;
		else
			System.out.println("Negative value for surface, value not changed");

	}

	/**
	 * Set the population of the country
	 * @param population The population of the country
	 */
	public void setPopulation(double population) {

		if (population > 0)
			this.population = population;
		else
			System.out.println("Negative value for population, value not changed");

	}

	/**
	 * Return a string representation of the country
	 */
	public String toString() {

		return "Pays: " + this.nom + "\nPopulation: " + this.population + "\nSurface: " + this.surface;

	}
	
}

/**
 * This class represents a fraction
 */
public class Rationnel {

	/**
	 * The fraction's numerator
	 */
	private int numerateur;

	/**
	 * The fraction's denominator
	 */
	private int denominateur;

	/**
	 * Default constructor which checks if the denominator is negative and makes it positive if it is.
	 * 
	 * @param num The fraction's numerator
	 * @param denom The fraction's denominator
	 */
	public Rationnel(int num, int denom) {

		
		if (denom < 0) {
			
			denom = denom * (-1);
			
			num = num * (-1);
			
		} else if (denom == 0) {

			System.out.println("The denominator can't be 0, it has been automatically set to 1.");
			
			denom = 1;
			
		}

		this.numerateur = num;

		this.denominateur = denom;

	}

	/**
	 * Returns the fraction's numerator
	 * 
	 * @return The fraction's numerator
	 */
	public int getNumerateur() {

		return this.numerateur;

	}

	/**
	 * Sets the fraction's numerator
	 * 
	 * @param num The fraction's numerator
	 */
	public void setNumerateur(int num) {

		this.numerateur = num;

	}

	/**
	 * Returns the fraction's denominator
	 * 
	 * @return The fraction's denominator
	 */
	public int getDenominateur() {

		return this.denominateur;

	}

	/**
	 * Sets the fraction's denominator and checks if it is negative and makes it positive if it is.
	 * 
	 * @param denom The fraction's denominator
	 */
	public void setDenominateur(int denom) {

		if (denom != 0) {

			if (denom < 0) {
	
				denom = denom * (-1);
	
				this.numerateur = this.numerateur * (-1);
	
			}
	
			this.denominateur = denom;

		} else {

			System.out.println("The denominator can't be 0, nothing was changed.");

		}

	}

	/**
	 * Reverses the fraction
	 * 
	 * @return The reversed fraction
	 */
	public Rationnel inverse() {

		int num = this.numerateur;

		int denom = this.denominateur;

		if (num < 0) {

			num = num * (-1);

			denom = denom * (-1);

		} else if (num == 0) {

			num = 1;

		}

		return new Rationnel(denom, num);

	}

	/**
	 * Adds the fraction to the one given in parameter
	 * 
	 * @param ratio The fraction to add
	 * @return The sum of the two fractions
	 */
	public Rationnel ajoute(Rationnel ratio) {

		Rationnel res = null;

		if (ratio != null) {

			int num = (this.numerateur * ratio.getDenominateur()) + (ratio.getNumerateur() * this.denominateur);
	
			int denom = this.denominateur * ratio.getDenominateur();

			if (denom < 0) {

				num = num * (-1);
	
				denom = denom * (-1);
	
			}
	
			res = new Rationnel(num, denom);
	
			res.reduit();

		} else {

			System.out.println("Null parameter not permitted.");

		}

		return res;

	}

	/**
	 * Subtracts the fraction given in parameter from the one of the object
	 * 
	 * @param ratio The fraction to subtract
	 * @return The difference of the two fractions
	 */
	public Rationnel soustrait(Rationnel ratio) {

		Rationnel res = null;

		if (ratio != null) {

			int num = (this.numerateur * ratio.getDenominateur()) - (ratio.getNumerateur() * this.denominateur);
	
			int denom = this.denominateur * ratio.getDenominateur();

			if (denom < 0) {

				num = num * (-1);
	
				denom = denom * (-1);
	
			}
	
			res = new Rationnel(num, denom);
	
			res.reduit();

		} else {

			System.out.println("Null parameter not permitted.");

		}

		return res;

	}

	/**
	 * Multiplies the fraction by the one given in parameter
	 * 
	 * @param ratio The fraction to multiply by
	 * @return The product of the two fractions
	 */
	public Rationnel multiplie(Rationnel ratio) {

		Rationnel res = null;

		if (ratio != null) {

			int denom = this.denominateur * ratio.getDenominateur();
	
			int num = this.numerateur * ratio.getNumerateur();

			if (denom < 0) {

				num = num * (-1);
	
				denom = denom * (-1);
	
			}

			res = new Rationnel(num, denom);

		} else {

			System.out.println("Null parameter not permitted.");

		}

		return res;

	}

	/**
	 * Checks if the fraction is equal to the one given in parameter
	 * 
	 * @param ratio The fraction to compare to
	 * @return True if the fractions are equal, false otherwise
	 */
	public boolean egale(Rationnel ratio) {

		boolean ret = false;

		if (ratio != null) {

			this.reduit();
	
			ratio.reduit();
	
			ret = (this.numerateur == ratio.getNumerateur()) && (this.denominateur == ratio.getDenominateur());

		} else {

			System.out.println("Null parameter not permitted.");

		}

		return ret;

	}

	/**
	 * Reduces the fraction
	 */
	private void reduit() {

		int diviseur = this.pgcd(this.numerateur, this.denominateur);

		if (diviseur < 0) {

			diviseur = diviseur * (-1);

		}

		this.numerateur = this.numerateur / diviseur;

		this.denominateur = this.denominateur / diviseur;

	}

	/**
	 * Returns the greatest common divisor of the two numbers given in parameter
	 * 
	 * @param a The first number
	 * @param b The second number
	 * @return The greatest common divisor of the two numbers
	 */
	private int pgcd(int a, int b) {

		int ret = a;

		if (b != 0) {

			ret = this.pgcd(b, (a % b));

		}

		return ret;

	}

	/**
	 * Returns the fraction as a string
	 * 
	 * @return The fraction as a string
	 */
	public String toString() {

		String ret = "";

		ret = ret + this.numerateur + "/" + this.denominateur;

		return ret;

	}

}
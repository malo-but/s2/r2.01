/**
 * This class tests the Rationnel class
 */
public class EssaiRationnel {

	/**
	 * The main method
	 * @param args The command line arguments
	 */
	public static void main(String[] args) {

		new EssaiRationnel();

	}

	/**
	 * The constructor
	 */
	public EssaiRationnel() {

		this.constructorTest();

		this.getNumerateurTest();

		this.setNumerateurTest();

		this.getDenominateurTest();

		this.setDenominateurTest();

		this.inverseTest();

		this.ajouteTest();

		this.soustraitTest();

		this.multiplieTest();

		this.egaleTest();

		this.toStringTest();

	}

	/**
	 * Tests the constructor
	 */
	private void constructorTest() {

		System.out.println("\n*** Testing Rationnel's constructor ***\n");

		System.out.println("\t- Normal Cases :");

		this.constructorTestCase(10, 5, 10, 5);

		this.constructorTestCase(5, 10, 5, 10);

		this.constructorTestCase(-10, 5, -10, 5);

		this.constructorTestCase(10, -5, -10, 5);

		System.out.println("\t- Edge Cases :");

		this.constructorTestCase(0, 5, 0, 5);

		System.out.print("\t\t");

		this.constructorTestCase(5, 0, 5, 1);

	}

	/**
	 * Tests the constructor with the given parameters
	 * @param numerateur The fraction's numerator
	 * @param denominateur The fraction's denominator
	 * @param expectedNumerateur The expected numerator
	 * @param expectedDenominateur The expected denominator
	 */
	private void constructorTestCase(int numerateur, int denominateur, int expectedNumerateur, int expectedDenominateur) {

		Rationnel ratio = new Rationnel(numerateur, denominateur);

		System.out.print("\t\tRationnel(" + numerateur + ", " + denominateur + ") = " + ratio);

		if ((ratio.getNumerateur() == expectedNumerateur) && (ratio.getDenominateur() == expectedDenominateur)) {

			System.out.println(" : OK");

		} else {

			System.out.println(" : ERROR | Expected : " + expectedNumerateur + "/" + expectedDenominateur);

		}

	}

	/**
	 * Tests the getNumerateur() method
	 */
	private void getNumerateurTest() {

		System.out.println("\n*** Testing Rationnel.getNumerateur() ***\n");

		System.out.println("\t- Normal Cases :");

		Rationnel ratio1 = new Rationnel(10, 5);

		this.getNumerateurTestCase(ratio1, 10);

		Rationnel ratio2 = new Rationnel(5, 10);

		this.getNumerateurTestCase(ratio2, 5);

		Rationnel ratio3 = new Rationnel(-10, 5);

		this.getNumerateurTestCase(ratio3, -10);

		Rationnel ratio4 = new Rationnel(10, -5);

		this.getNumerateurTestCase(ratio4, -10);

		System.out.println("\t- Edge Cases :");

		Rationnel ratio5 = new Rationnel(0, 5);

		this.getNumerateurTestCase(ratio5, 0);

	}

	/**
	 * Tests the getNumerateur() method with the given parameters
	 * @param ratio The fraction
	 * @param expected The expected numerator
	 */
	private void getNumerateurTestCase(Rationnel ratio, int expected) {

		int res = ratio.getNumerateur();

		System.out.print("\t\tRationnel.getNumerateur() = " + res);

		if (res == expected) {

			System.out.println(" : OK");

		} else {

			System.out.println(" : ERROR | Expected : " + expected);

		}

	}

	/**
	 * Tests the setNumerateur() method
	 */
	private void setNumerateurTest() {

		System.out.println("\n*** Testing Rationnel.setNumerateur() ***\n");

		System.out.println("\t- Normal Cases :");

		Rationnel ratio1 = new Rationnel(10, 5);

		this.setNumerateurTestCase(ratio1, 5, 5);

		Rationnel ratio2 = new Rationnel(5, 10);

		this.setNumerateurTestCase(ratio2, 10, 10);

		Rationnel ratio3 = new Rationnel(-10, 5);

		this.setNumerateurTestCase(ratio3, 5, 5);

		Rationnel ratio4 = new Rationnel(10, -5);

		this.setNumerateurTestCase(ratio4, -5, -5);

		System.out.println("\t- Edge Cases :");

		Rationnel ratio5 = new Rationnel(0, 5);

		this.setNumerateurTestCase(ratio5, 0, 0);

	}

	/**
	 * Tests the setNumerateur() method with the given parameters
	 * @param ratio The fraction
	 * @param numerateur The new numerator
	 * @param expectedNumerateur The expected numerator
	 */
	private void setNumerateurTestCase(Rationnel ratio, int numerateur, int expectedNumerateur) {

		ratio.setNumerateur(numerateur);

		System.out.print("\t\tRationnel.setNumerateur(" + numerateur + ") = " + ratio);

		if (ratio.getNumerateur() == expectedNumerateur) {

			System.out.println(" : OK");

		} else {

			System.out.println(" : ERROR | Expected : " + expectedNumerateur);

		}

	}

	/**
	 * Tests the getDenominateur() method
	 */
	private void getDenominateurTest() {

		System.out.println("\n*** Testing Rationnel.getDenominateur() ***\n");

		System.out.println("\t- Normal Cases :");

		Rationnel ratio1 = new Rationnel(10, 5);

		this.getDenominateurTestCase(ratio1, 5);

		Rationnel ratio2 = new Rationnel(5, 10);

		this.getDenominateurTestCase(ratio2, 10);

		Rationnel ratio3 = new Rationnel(-10, 5);

		this.getDenominateurTestCase(ratio3, 5);

		Rationnel ratio4 = new Rationnel(10, -5);

		this.getDenominateurTestCase(ratio4, 5);

		System.out.println("\t- Edge Cases :");

		Rationnel ratio5 = new Rationnel(0, 5);

		this.getDenominateurTestCase(ratio5, 5);

	}

	/**
	 * Tests the getDenominateur() method with the given parameters
	 * @param ratio The fraction
	 * @param expected The expected denominator
	 */
	private void getDenominateurTestCase(Rationnel ratio, int expected) {

		int res = ratio.getDenominateur();

		System.out.print("\t\tRationnel.getDenominateur() = " + res);

		if (res == expected) {

			System.out.println(" : OK");

		} else {

			System.out.println(" : ERROR | Expected : " + expected);

		}

	}

	/**
	 * Tests the setDenominateur() method
	 */
	private void setDenominateurTest() {

		System.out.println("\n*** Testing Rationnel.setDenominateur() ***\n");

		System.out.println("\t- Normal Cases :");

		Rationnel ratio1 = new Rationnel(10, 5);

		this.setDenominateurTestCase(ratio1, 10, 10);

		Rationnel ratio2 = new Rationnel(5, 10);

		this.setDenominateurTestCase(ratio2, 5, 5);

		Rationnel ratio3 = new Rationnel(-10, 5);

		this.setDenominateurTestCase(ratio3, 10, 10);

		Rationnel ratio4 = new Rationnel(10, -5);

		this.setDenominateurTestCase(ratio4, 10, 10);

		System.out.println("\t- Edge Cases :");

		System.out.print("\t\t");

		Rationnel ratio5 = new Rationnel(5, 5);

		this.setDenominateurTestCase(ratio5, 0, 5);

	}

	/**
	 * Tests the setDenominateur() method with the given parameters
	 * @param ratio The fraction
	 * @param denominateur The new denominator
	 * @param expectedDenominateur The expected denominator
	 */
	private void setDenominateurTestCase(Rationnel ratio, int denominateur, int expectedDenominateur) {

		ratio.setDenominateur(denominateur);

		System.out.print("\t\tRationnel.setDenominateur(" + denominateur + ") = " + ratio);

		if (ratio.getDenominateur() == expectedDenominateur) {

			System.out.println(" : OK");

		} else {

			System.out.println(" : ERROR | Expected : " + expectedDenominateur);

		}

	}

	/**
	 * Tests the inverse() method
	 */
	private void inverseTest() {

		System.out.println("\n*** Testing Rationnel.inverse() ***\n");

		System.out.println("\t- Normal Cases :");

		Rationnel ratio1 = new Rationnel(10, 5);

		Rationnel expected1 = new Rationnel(5, 10);

		this.inverseTestCase(ratio1, expected1);

		Rationnel ratio2 = new Rationnel(5, 10);

		Rationnel expected2 = new Rationnel(10, 5);

		this.inverseTestCase(ratio2, expected2);

		Rationnel ratio3 = new Rationnel(-10, 5);

		Rationnel expected3 = new Rationnel(5, -10);

		this.inverseTestCase(ratio3, expected3);

		System.out.println("\t- Edge Cases :");

		Rationnel ratio4 = new Rationnel(0, 5);

		Rationnel expected4 = new Rationnel(5, 1);

		this.inverseTestCase(ratio4, expected4);


	}

	/**
	 * Tests the inverse() method with the given parameters
	 * @param ratio The fraction
	 * @param expected The expected inverse
	 */
	private void inverseTestCase(Rationnel ratio, Rationnel expected) {

		Rationnel res = ratio.inverse();

		System.out.print("\t\tRationnel.inverse() = " + res);

		if ((res.getNumerateur() == expected.getNumerateur()) && (res.getDenominateur() == expected.getDenominateur())) {

			System.out.println(" : OK");

		} else {

			System.out.println(" : ERROR | Expected : " + expected);

		}

	}

	/**
	 * Tests the ajoute() method
	 */
	private void ajouteTest() {

		System.out.println("\n*** Testing Rationnel.ajoute() ***\n");

		System.out.println("\t- Normal Cases :");

		Rationnel ratio1 = new Rationnel(1, 2);

		Rationnel ratio2 = new Rationnel(1, 3);

		Rationnel expected1 = new Rationnel(5, 6);

		this.ajouteTestCase(ratio1, ratio2, expected1);

		Rationnel ratio3 = new Rationnel(1, 2);

		Rationnel ratio4 = new Rationnel(1, 2);

		Rationnel expected2 = new Rationnel(1, 1);

		this.ajouteTestCase(ratio3, ratio4, expected2);

		Rationnel ratio5 = new Rationnel(1, 2);

		Rationnel ratio6 = new Rationnel(1, 4);

		Rationnel expected3 = new Rationnel(3, 4);

		this.ajouteTestCase(ratio5, ratio6, expected3);

		System.out.println("\t- Edge Cases :");

		Rationnel ratio7 = new Rationnel(0, 2);

		Rationnel ratio8 = new Rationnel(1, 4);

		Rationnel expected4 = new Rationnel(1, 4);

		this.ajouteTestCase(ratio7, ratio8, expected4);

		Rationnel ratio9 = new Rationnel(1, 2);

		Rationnel ratio10 = new Rationnel(0, 4);

		Rationnel expected5 = new Rationnel(1, 2);

		this.ajouteTestCase(ratio9, ratio10, expected5);

		Rationnel ratio11 = new Rationnel(0, 2);

		Rationnel ratio12 = new Rationnel(-2, 4);

		Rationnel expected6 = new Rationnel(-1, 2);

		this.ajouteTestCase(ratio11, ratio12, expected6);

		System.out.println("\t- Error Cases :");

		System.out.print("\t\t");

		Rationnel ratio13 = new Rationnel(1, 2);

		this.ajouteTestCase(ratio13, null, null);

	}

	/**
	 * Tests the ajoute() method with the given parameters
	 * @param ratio1 The first fraction
	 * @param ratio2 The second fraction
	 * @param expected The expected result
	 */
	private void ajouteTestCase(Rationnel ratio1, Rationnel ratio2, Rationnel expected) {

		Rationnel res = ratio1.ajoute(ratio2);

		System.out.print("\t\tRationnel.ajoute(" + ratio2 + ") = " + res);

		if (res == null) {

			if (expected == null) {

				System.out.println(" : OK");

			} else {

				System.out.println(" : ERROR | Expected : " + expected);

			}

		} else {

			if ((res.getNumerateur() == expected.getNumerateur()) && (res.getDenominateur() == expected.getDenominateur())) {
	
				System.out.println(" : OK");
	
			} else {
	
				System.out.println(" : ERROR | Expected : " + expected);
	
			}

		}

	}

	/**
	 * Tests the soustrait() method
	 */
	private void soustraitTest() {

		System.out.println("\n*** Testing Rationnel.soustrait() ***\n");

		System.out.println("\t- Normal Cases :");

		Rationnel ratio1 = new Rationnel(1, 2);

		Rationnel ratio2 = new Rationnel(1, 3);

		Rationnel expected1 = new Rationnel(1, 6);

		this.soustraitTestCase(ratio1, ratio2, expected1);

		Rationnel ratio3 = new Rationnel(1, 2);

		Rationnel ratio4 = new Rationnel(1, 2);

		Rationnel expected2 = new Rationnel(0, 1);

		this.soustraitTestCase(ratio3, ratio4, expected2);

		Rationnel ratio5 = new Rationnel(1, 2);

		Rationnel ratio6 = new Rationnel(1, 4);

		Rationnel expected3 = new Rationnel(1, 4);

		this.soustraitTestCase(ratio5, ratio6, expected3);

		System.out.println("\t- Edge Cases :");

		Rationnel ratio7 = new Rationnel(0, 2);

		Rationnel ratio8 = new Rationnel(2, 4);

		Rationnel expected4 = new Rationnel(-1, 2);

		this.soustraitTestCase(ratio7, ratio8, expected4);

		Rationnel ratio9 = new Rationnel(1, 2);

		Rationnel ratio10 = new Rationnel(0, 4);

		Rationnel expected5 = new Rationnel(1, 2);

		this.soustraitTestCase(ratio9, ratio10, expected5);

		System.out.println("\t- Error Cases :");

		System.out.print("\t\t");

		Rationnel ratio11 = new Rationnel(1, 2);

		this.soustraitTestCase(ratio11, null, null);

	}

	/**
	 * Tests the soustrait() method with the given parameters
	 * @param ratio1 The first fraction
	 * @param ratio2 The second fraction
	 * @param expected The expected result
	 */
	private void soustraitTestCase(Rationnel ratio1, Rationnel ratio2, Rationnel expected) {

		Rationnel res = ratio1.soustrait(ratio2);

		System.out.print("\t\tRationnel.soustrait(" + ratio2 + ") = " + res);

		if (res == null) {

			if (expected == null) {

				System.out.println(" : OK");

			} else {

				System.out.println(" : ERROR | Expected : " + expected);

			}

		} else {

			if ((res.getNumerateur() == expected.getNumerateur()) && (res.getDenominateur() == expected.getDenominateur())) {
	
				System.out.println(" : OK");
	
			} else {
	
				System.out.println(" : ERROR | Expected : " + expected);
	
			}

		}

	}

	/**
	 * Tests the multiplie() method
	 */
	private void multiplieTest() {

		System.out.println("\n*** Testing Rationnel.multiplie() ***\n");

		System.out.println("\t- Normal Cases :");

		Rationnel ratio1 = new Rationnel(1, 2);

		Rationnel ratio2 = new Rationnel(1, 3);

		Rationnel expected1 = new Rationnel(1, 6);

		this.multiplieTestCase(ratio1, ratio2, expected1);

		Rationnel ratio3 = new Rationnel(1, 2);

		Rationnel ratio4 = new Rationnel(1, 2);

		Rationnel expected2 = new Rationnel(1, 4);

		this.multiplieTestCase(ratio3, ratio4, expected2);

		Rationnel ratio5 = new Rationnel(1, 2);

		Rationnel ratio6 = new Rationnel(1, 4);

		Rationnel expected3 = new Rationnel(1, 8);

		this.multiplieTestCase(ratio5, ratio6, expected3);

		System.out.println("\t- Edge Cases :");

		Rationnel ratio7 = new Rationnel(0, 2);

		Rationnel ratio8 = new Rationnel(2, 4);

		Rationnel expected4 = new Rationnel(0, 8);

		this.multiplieTestCase(ratio7, ratio8, expected4);

		Rationnel ratio9 = new Rationnel(1, 2);

		Rationnel ratio10 = new Rationnel(0, 4);

		Rationnel expected5 = new Rationnel(0, 8);

		this.multiplieTestCase(ratio9, ratio10, expected5);

		System.out.println("\t- Error Cases :");

		System.out.print("\t\t");

		Rationnel ratio11 = new Rationnel(1, 2);

		this.multiplieTestCase(ratio11, null, null);

	}

	/**
	 * Tests the multiplie() method with the given parameters
	 * @param ratio1 The first fraction
	 * @param ratio2 The second fraction
	 * @param expected The expected result
	 */
	private void multiplieTestCase(Rationnel ratio1, Rationnel ratio2, Rationnel expected) {

		Rationnel res = ratio1.multiplie(ratio2);

		System.out.print("\t\tRationnel.multiplie(" + ratio2 + ") = " + res);

		if (res == null) {

			if (expected == null) {

				System.out.println(" : OK");

			} else {

				System.out.println(" : ERROR | Expected : " + expected);

			}

		} else {

			if ((res.getNumerateur() == expected.getNumerateur()) && (res.getDenominateur() == expected.getDenominateur())) {
	
				System.out.println(" : OK");
	
			} else {
	
				System.out.println(" : ERROR | Expected : " + expected);
	
			}

		}

	}

	/**
	 * Tests the divise() method
	 */
	private void egaleTest() {

		System.out.println("\n*** Testing Rationnel.egale() ***\n");

		System.out.println("\t- Normal Cases :");

		Rationnel ratio1 = new Rationnel(1, 2);

		Rationnel ratio2 = new Rationnel(4, 8);

		egaleTestCase(ratio1, ratio2, true);

		Rationnel ratio3 = new Rationnel(1, 2);

		Rationnel ratio4 = new Rationnel(3, 6);

		egaleTestCase(ratio3, ratio4, true);

		Rationnel ratio5 = new Rationnel(1, 2);

		Rationnel ratio6 = new Rationnel(1, 3);

		egaleTestCase(ratio5, ratio6, false);

		Rationnel ratio7 = new Rationnel(1, 2);

		Rationnel ratio8 = new Rationnel(2, 3);

		egaleTestCase(ratio7, ratio8, false);

		System.out.println("\t- Edge Cases :");

		Rationnel ratio9 = new Rationnel(0, 2);

		Rationnel ratio10 = new Rationnel(0, 3);

		egaleTestCase(ratio9, ratio10, true);

		System.out.println("\t- Error Cases :");

		System.out.print("\t\t");

		Rationnel ratio11 = new Rationnel(1, 2);

		this.egaleTestCase(ratio11, null, false);

	}

	/**
	 * Tests the egale() method with the given parameters
	 * @param ratio1 The first fraction
	 * @param ratio2 The second fraction
	 * @param expected The expected result
	 */
	private void egaleTestCase(Rationnel ratio1, Rationnel ratio2, boolean expected) {

		boolean res = ratio1.egale(ratio2);

		System.out.print("\t\tRationnel.egale() = " + res);

		if (res == expected) {

			System.out.println(" : OK");

		} else {

			System.out.println(" : ERROR | Expected : " + expected);

		}

	}

	/**
	 * Tests the toString() method
	 */
	private void toStringTest() {

		System.out.println("\n*** Testing Rationnel.toString() ***\n");

		System.out.println("\t- Normal Cases :");

		Rationnel ratio1 = new Rationnel(1, 2);

		toStringTestCase(ratio1, "1/2");

		Rationnel ratio2 = new Rationnel(4, 8);

		toStringTestCase(ratio2, "4/8");

		Rationnel ratio3 = new Rationnel(1, 3);

		toStringTestCase(ratio3, "1/3");

		Rationnel ratio4 = new Rationnel(3, 6);

		toStringTestCase(ratio4, "3/6");

		System.out.println("\t- Edge Cases :");

		Rationnel ratio5 = new Rationnel(0, 2);

		toStringTestCase(ratio5, "0/2");

	}

	/**
	 * Tests the toString() method with the given parameters
	 * @param ratio The fraction
	 * @param expected The expected result
	 */
	private void toStringTestCase(Rationnel ratio, String expected) {

		String res = ratio.toString();

		System.out.print("\t\tRationnel.toString() = " + res);

		if (res.equals(expected)) {

			System.out.println(" : OK");

		} else {

			System.out.println(" : ERROR | Expected : " + expected);

		}

	}
	
}

/**
 * Class representing a student.
 */
public class Etudiant {
	
	/**
	 * The name of the student.
	 */
	private String nom;

	/**
	 * The bulletin of the student.
	 */
	private double[][] bulletin;

	/**
	 * The list of the subjects.
	 */
	private final String[] MATIERES;

	/**
	 * The list of the coefficients.
	 */
	private final double[] COEFFICIENTS;

	/**
	 * Constructor of the class.
	 * @param nom The name of the student.
	 * @param matieres The list of the subjects.
	 * @param coeff The list of the coefficients.
	 * @param nbNotes The number of marks.
	 */
	public Etudiant(String nom, String[] matieres, double[] coeff, int nbNotes) {

		if (nom != null && nom.length() > 0) {

			this.nom = nom;

		} else {

			System.out.println("Nom invalide.");

			this.nom = "";

		}

		String[] newMatieres = new String[0];

		double[] newCoeff = new double[0];

		if (matieres != null && coeff != null) {
	
			if (matieres.length > coeff.length) {
	
				newMatieres = new String[coeff.length];
	
				for (int i = 0; i < coeff.length; i++) {
	
					newMatieres[i] = matieres[i];
	
				}
	
			} else if (matieres.length < coeff.length) {
	
				newCoeff = new double[matieres.length];
	
				for (int i = 0; i < matieres.length; i++) {
	
					newCoeff[i] = coeff[i];
	
				}
	
			} else {

				newMatieres = matieres;

				newCoeff = coeff;

			}

		} else {

			System.out.println("Matières ou coefficients nuls.");

		}

		if (nbNotes <= 0) {

			nbNotes = 1;

		}

		this.MATIERES = newMatieres;

		this.COEFFICIENTS = newCoeff;

		this.bulletin = new double[this.MATIERES.length][nbNotes];

		this.initialisation();

	}

	/**
	 * Modifies the name of the student.
	 * @param nom The new name of the student.
	 */
	public void setNom(String nom) {

		if (nom == null || nom.length() == 0) {

			System.out.println("Nom invalide");

		} else {
			
			this.nom = nom;

		}

	}

	/**
	 * Returns the name of the student.
	 * @return The name of the student.
	 */
	public String getNom() {

		return this.nom;

	}

	/**
	 * Returns the number of subjects.
	 * @return The number of subjects.
	 */
	public int getNbMatieres() {

		return this.MATIERES.length;

	}

	/**
	 * Returns the mark the student got for a given subject.
	 * @param indMatiere The index of the subject.
	 * @param ind The index of the mark.
	 * @return The mark the student got for a given subject.
	 */
	public double getUneNote(int indMatiere, int ind) {

		double ret = -1;

		if (indMatiere < 0 || indMatiere > bulletin.length) {

			System.out.println("Indice de matière invalide.");

		} else if (ind < 0 || ind > bulletin[indMatiere].length) {

			System.out.println("Indice de la note invalide.");

		} else {

			ret = bulletin[indMatiere][ind];

		}

		return ret;

	}

	/**
	 * Initializes the bulletin of the student.
	 */
	private void initialisation() {

		for (int i = 0; i < this.bulletin.length; i++) {

			for (int j = 0; j < this.bulletin[i].length; j++) {

				double note = Math.random() * 21;

				if (note > 20) {

					note = 20;

				}

				this.bulletin[i][j] = this.reduireDecimal(note, 2);

			}

		}

	}

	/**
	 * Calculates the average mark of a given subject.
	 * @param indMatiere The index of the subject.
	 * @return The average mark of a given subject.
	 */
	public double moyenneMatiere(int indMatiere) {

		double sum = -1;

		if (indMatiere < 0 || indMatiere > this.bulletin.length) {

			System.out.println("Indice invalide.");

		} else {

			sum = 0;

			for (int i = 0; i < this.bulletin[indMatiere].length; i++) {

				sum += this.bulletin[indMatiere][i];

			}

			sum /= this.bulletin[indMatiere].length;

			sum = this.reduireDecimal(sum, 2);

		}

		return sum;

	}

	/**
	 * Calculates the average mark of the student.
	 * @return The average mark of the student.
	 */
	public double moyenneGenerale() {

		double sum = 0;
		double sumCoeff = 0;

		for (int i = 0; i < this.MATIERES.length; i++) {

			sum += this.moyenneMatiere(i) * this.COEFFICIENTS[i];

			sumCoeff += this.COEFFICIENTS[i];

		}

		sum /= sumCoeff;

		sum = this.reduireDecimal(sum, 2);

		return sum;

	}

	/**
	 * Returns the subject with the best mark.
	 * @return The subject with the best mark.
	 */
	public String meilleureNote() {

		String bestMatiere = this.MATIERES[0];

		double bestNote = -1;

		for (int i = 0; i < this.bulletin.length; i++) {

			for (int j = 0; j < this.bulletin[i].length; j++) {

				if (this.bulletin[i][j] > bestNote) {

					bestMatiere = this.MATIERES[i];
					bestNote = this.bulletin[i][j];

				}

			}

		}

		return "Meilleure note : " + bestMatiere + " - " + bestNote;

	}

	/**
	 * Returns a readable version of the bulletin.
	 * @return A readable version of the bulletin.
	 */
	public String toString() {

		String ret = "NOM : " + this.nom + "\nMOYENNE GENERALE : " + this.moyenneGenerale() + "\nMATIERE\t\tCOEFF\tMOYENNE\t\tNOTES\n";

		for (int i = 0; i < this.bulletin.length; i++) {

			ret += this.MATIERES[i] + "\t\t" + this.COEFFICIENTS[i] + "\t" + this.moyenneMatiere(i) + "\t\t";

			for (int j = 0; j < this.bulletin[i].length; j++) {

				ret += this.bulletin[i][j] + "\t";

			}

			ret += "\n";

		}

		return ret;

	}

	/**
	 * Reduces the number of decimals of a given number.
	 * @param valeur The number to reduce.
	 * @param nbDecimal The number of decimals to keep.
	 * @return The number with the reduced number of decimals.
	 */
	private double reduireDecimal(double valeur, int nbDecimal) {

		valeur = valeur * Math.pow(10, nbDecimal);

		valeur = Math.round(valeur);

		valeur = valeur / Math.pow(10, nbDecimal);

		return valeur;

	}

	/**
	 * Returns the bulletin of the student.
	 * @return The bulletin of the student.
	 */
	public double[][] getBulletin() {

		return this.bulletin;

	}

	/**
	 * Returns the subjects.
	 * @return The subjects.
	 */
	public String[] getMATIERES() {

		return this.MATIERES;

	}

	/**
	 * Returns the coefficients.
	 * @return The coefficients.
	 */
	public double[] getCOEFFICIENTS() {

		return this.COEFFICIENTS;

	}

}

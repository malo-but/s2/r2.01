/**
 * Test class for the Promotion2 class
 */
public class ScenarioPromotion2 {

	/**
	 * The list of students in the promotion
	 */
	Etudiant[] listeEtudiants = new Etudiant[30];

	/**
	 * Main method
	 * 
	 * @param args The command line arguments
	 */
	public static void main(String[] args) {

		new ScenarioPromotion2();

	}

	/**
	 * Constructor for the test class
	 */
	public ScenarioPromotion2() {
		
		this.genererEtudiants();


		this.constructorTest();

		this.inscrireTest();

	}

	/**
	 * Generates a list of students
	 */
	private void genererEtudiants() {

		String[] matieres = {"Maths", "Physique", "Chimie", "SVT", "Français", "Anglais", "Histoire", "Géographie", "EPS", "Arts plastiques"};

		double[] coeff = new double[matieres.length];

		for (int i = 0; i < coeff.length; i++) {

			coeff[i] = (int) (Math.random() * 4) + 1;

		}

		for (int i = 0; i < 30; i++) {

			this.listeEtudiants[i] = new Etudiant("Etudiant " + i, matieres, coeff, 3);

		}

	}

	/**
	 * Tests the constructor
	 */
	private void constructorTest() {

		System.out.println("\n*** Test du constructeur ***\n");

		System.out.println("Cas normaux : ");

		constructorTestCase("Promotion 1", 30);

		constructorTestCase("Promotion 2", -2);

		constructorTestCase(null, 30);

		constructorTestCase(null, -2);

	}

	/**
	 * Tests the constructor with a specific case
	 * 
	 * @param nom The name of the promotion
	 * @param nbEtudiants The number of students
	 */
	private void constructorTestCase(String nom, int nbEtudiants) {

		System.out.print("Promotion(\"" + nom + "\"," + nbEtudiants + ") = ");

		new Promotion2(nom, nbEtudiants);

		System.out.println("Done");

	}

	/**
	 * Tests the inscrire method
	 */
	private void inscrireTest() {

		System.out.println("\n*** Test de la méthode inscrire ***\n");

		System.out.println("Cas normaux : ");

		Promotion2 promotion = new Promotion2("Promotion 1", 30);

		inscrireTestCase(true, promotion, this.listeEtudiants[0]);

		inscrireTestCase(true, promotion, this.listeEtudiants[1]);

		Promotion2 promotion2 = new Promotion2("Promotion 2", this.listeEtudiants);

		inscrireTestCase(false, promotion2, this.listeEtudiants[0]);

		Promotion2 promotion3 = new Promotion2("Promotion 3", -2);

		inscrireTestCase(false, promotion3, this.listeEtudiants[0]);
		
	}

	/**
	 * Tests the inscrire method with a specific case
	 * 
	 * @param expected The expected result
	 * @param promotion The promotion
	 * @param etudiant The student
	 */
	private void inscrireTestCase(boolean expected, Promotion2 promotion, Etudiant etudiant) {
		
		boolean res = promotion.inscrire(etudiant);
		
		System.out.print("promotion.inscrire() = " + res);

		if (res == expected) {

			System.out.println(" : OK");

		} else {

			System.out.println(" : Erreur");

		}

	}
	
}

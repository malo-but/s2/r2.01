/**
 * Test class for Promotion
 */
public class ScenarioPromotion {

	/**
	 * The list of students
	 */
	Etudiant[] listeEtudiants = new Etudiant[30];

	/**
	 * Main method
	 * 
	 * @param args The command line arguments
	 */
	public static void main(String[] args) {

		new ScenarioPromotion();

	}

	/**
	 * Constructor for the test class
	 */
	public ScenarioPromotion() {

		this.genererEtudiants();


		this.constructorTest();

		this.getNomTest();

		this.setNomTest();

		this.moyenneTest();

		this.moyenneMaxTest();

		this.moyenneMinTest();

		this.moyenneMatiereTest();

		this.getMajorTest();

		this.getEtudiantTest();

		this.toStringTest();

	}

	/**
	 * Generates a list of students
	 */
	private void genererEtudiants() {

		String[] matieres = {"Maths", "Physique", "Chimie", "SVT", "Français", "Anglais", "Histoire", "Géographie", "EPS", "Arts plastiques"};

		double[] coeff = new double[matieres.length];

		for (int i = 0; i < coeff.length; i++) {

			coeff[i] = (int) (Math.random() * 4) + 1;

		}

		for (int i = 0; i < 30; i++) {

			this.listeEtudiants[i] = new Etudiant("Etudiant " + i, matieres, coeff, 3);

		}

	}

	/**
	 * Tests the constructor
	 */
	private void constructorTest() {

		System.out.println("\n*** Test du constructeur ***\n");

		System.out.println("Cas normaux : ");

		constructorTestCase("Promotion 1", this.listeEtudiants);

		constructorTestCase("Promotion 2", null);

		constructorTestCase(null, this.listeEtudiants);
		
		constructorTestCase(null, null);

	}

	/**
	 * Tests the constructor with a specific case
	 * 
	 * @param nom The name of the promotion
	 * @param listeEtudiants The list of students
	 */
	private void constructorTestCase(String nom, Etudiant[] listeEtudiants) {

		System.out.print("Promotion(\"" + nom + "\"," + listeEtudiants + ") = ");

		new Promotion(nom, listeEtudiants);

		System.out.println("Done");

	}

	/**
	 * Tests the getNom method
	 */
	private void getNomTest() {

		System.out.println("\n*** Test de getNom ***\n");

		System.out.println("Cas normaux : ");

		getNomTestCase("Promotion 1");

		getNomTestCase("Promotion 2");

		System.out.println("Cas limites : ");

		getNomTestCase("");

		getNomTestCase(null);

	}

	/**
	 * Tests the getNom method with a specific case
	 * 
	 * @param nom The name of the promotion
	 */
	private void getNomTestCase(String nom) {

		System.out.print("Promotion(\"" + nom + "\").getNom() = ");

		Promotion promotion = new Promotion(nom, this.listeEtudiants);

		if (nom == null) {

			nom = "";

		}

		if (promotion.getNom().equals(nom)) {
			
			System.out.println("OK");

		} else {

			System.out.println("Erreur");

		}

	}

	/**
	 * Tests the setNom method
	 */
	private void setNomTest() {

		System.out.println("\n*** Test de setNom ***\n");

		System.out.println("Cas normaux : ");

		setNomTestCase("Promotion 1");

		setNomTestCase("Promotion 2");

		System.out.println("Cas limites : ");

		setNomTestCase("");

		setNomTestCase(null);

	}	

	/**
	 * Tests the setNom method with a specific case
	 * 
	 * @param nom The name of the promotion
	 */
	private void setNomTestCase(String nom) {

		System.out.print("Promotion(\"Promotion\").setNom(\"" + nom + "\") = ");

		Promotion promotion = new Promotion("Promotion", this.listeEtudiants);

		promotion.setNom(nom);

		if (nom == null) {

			nom = promotion.getNom();

		}

		if (promotion.getNom().equals(nom)) {
			
			System.out.println("OK");

		} else {

			System.out.println("Erreur");

		}

	}

	/**
	 * Tests the moyenne method
	 */
	private void moyenneTest() {

		System.out.println("\n*** Test de moyenne ***\n");

		System.out.println("Cas normaux : ");

		System.out.println("10 moyennes aléatoires devraient apparaître : ");

		for (int i = 0; i < 10; i++) {

			this.genererEtudiants();

			Promotion promotion = new Promotion("Promotion", this.listeEtudiants);

			System.out.println(promotion.moyenne());

		}

	}

	/**
	 * Tests the moyenneMax method
	 */
	private void moyenneMaxTest() {

		System.out.println("\n*** Test de moyenneMax ***\n");

		System.out.println("Cas normaux : ");

		System.out.println("10 meilleures moyennes aléatoires devraient apparaître : ");

		for (int i = 0; i < 10; i++) {

			this.genererEtudiants();

			Promotion promotion = new Promotion("Promotion", this.listeEtudiants);

			System.out.println(promotion.moyenneMax());

		}

	}

	/**
	 * Tests the moyenneMin method
	 */
	private void moyenneMinTest() {

		System.out.println("\n*** Test de moyenneMin ***\n");

		System.out.println("Cas normaux : ");

		System.out.println("10 pires moyennes aléatoires devraient apparaître : ");

		for (int i = 0; i < 10; i++) {

			this.genererEtudiants();

			Promotion promotion = new Promotion("Promotion", this.listeEtudiants);

			System.out.println(promotion.moyenneMin());

		}

	}

	/**
	 * Tests the moyenneMatiere method
	 */
	private void moyenneMatiereTest() {

		System.out.println("\n*** Test de moyenneMatiere ***\n");

		System.out.println("Cas normaux : ");

		System.out.println("10 moyennes aléatoires devraient apparaître : ");

		for (int i = 0; i < 10; i++) {

			this.genererEtudiants();

			Promotion promotion = new Promotion("Promotion", this.listeEtudiants);

			System.out.println(promotion.moyenneMatiere(0));

		}
	
	}

	/**
	 * Tests the getMajor method
	 */
	private void getMajorTest() {

		System.out.println("\n*** Test de getMajor ***\n");

		System.out.println("Cas normaux : ");

		for (int i = 0; i < 10; i++) {

			this.genererEtudiants();

			Etudiant major = this.listeEtudiants[0];

			for (int j = 1; j < this.listeEtudiants.length; j++) {

				if (this.listeEtudiants[j].moyenneGenerale() > major.moyenneGenerale()) {

					major = this.listeEtudiants[j];

				}

			}

			Promotion promotion = new Promotion("Promotion", this.listeEtudiants);

			System.out.print("getMajor() = ");

			if (promotion.getMajor() == major) {

				System.out.println("OK");

			} else {

				System.out.println("Erreur");

			}

		}
	
	}

	/**
	 * Tests the getEtudiant method
	 */
	private void getEtudiantTest() {

		System.out.println("\n*** Test de getEtudiant ***\n");

		System.out.println("Cas normaux : ");

		for (int i = 0; i < 10; i++) {

			Promotion promotion = new Promotion("Promotion", this.listeEtudiants);

			System.out.print("getEtudiant(Etudiant " + i + ") = ");

			if (promotion.getEtudiant("Etudiant " + i) == this.listeEtudiants[i]) {

				System.out.println("OK");

			} else {

				System.out.println("Erreur");

			}

		}

	}

	/**
	 * Tests the toString method
	 */
	private void toStringTest() {

		System.out.println("\n*** Test de toString ***\n");

		System.out.println("Cas normaux : ");

		System.out.println("5 promotions aléatoires devraient apparaître : ");

		for (int i = 0; i < 5; i++) {

			this.genererEtudiants();

			Promotion promotion = new Promotion("Promotion", this.listeEtudiants);

			System.out.println(promotion);

		} 

	}

}

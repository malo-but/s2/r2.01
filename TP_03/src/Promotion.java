/**
 * Class representing a student promotion
 */
public class Promotion {

	/**
	 * The promotion's name
	 */
	private String nom;

	/**
	 * The list of students in the promotion
	 */
	private Etudiant[] listeEtudiants;

	/**
	 * Constructor for the promotion class
	 * 
	 * @param nom The promotion's name
	 * @param liste The list of students in the promotion
	 */
	public Promotion(String nom, Etudiant[] liste) {

		if (nom != null) {

			this.nom = nom;

		} else {

			System.out.println("The name can't be null");

			this.nom = "";

		}

		if (liste != null) {

			this.listeEtudiants = liste;

		} else {

			System.out.println("The list of students can't be null");

			this.listeEtudiants = new Etudiant[0];

		}

	}

	/**
	 * Returns the promotion's name
	 * 
	 * @return The promotion's name
	 */
	public String getNom() {

		return this.nom;

	}

	/**
	 * Modifies the promotion's name
	 * 
	 * @param nom The new name
	 */
	public void setNom(String nom) {

		if (nom != null) {

			this.nom = nom;

		} else {

			System.out.println("The name can't be null, nothing was changed");

		}

	}

	/**
	 * Returns the promotion's average
	 */
	public double moyenne() {

		double moyenne = -1;

		if (this.listeEtudiants.length != 0) {

			moyenne = 0;

			for (int i = 0; i < this.listeEtudiants.length; i++) {
	
				moyenne += this.listeEtudiants[i].moyenneGenerale();
	
			}

			moyenne /= this.listeEtudiants.length;

		}


		return moyenne;

	}

	/**
	 * Returns the promotion's best average
	 * 
	 * @return The promotion's best average
	 */
	public double moyenneMax() {

		double max = 0;

		for (int i = 0; i < this.listeEtudiants.length; i++) {

			if (this.listeEtudiants[i].moyenneGenerale() > max) {

				max = this.listeEtudiants[i].moyenneGenerale();

			}

		}

		return max;

	}

	/**
	 * Returns the promotion's worst average
	 * 
	 * @return The promotion's worst average
	 */
	public double moyenneMin() {

		double min = 0;

		for (int i = 0; i < this.listeEtudiants.length; i++) {

			if (this.listeEtudiants[i].moyenneGenerale() < min) {

				min = this.listeEtudiants[i].moyenneGenerale();

			}

		}

		return min;

	}


	/**
	 * Returns the promotion average on one subject
	 * 
	 * @param index The index of the desired subject
	 * @return The promotion's average on the desired subject
	 */
	public double moyenneMatiere(int index) {

		double moyenne = -1;

		if (this.listeEtudiants.length != 0) {

			moyenne = 0;

			for (int i = 0; i < this.listeEtudiants.length; i++) {
	
				moyenne += this.listeEtudiants[i].moyenneMatiere(index);
	
			}
	
			moyenne /= this.listeEtudiants.length;

		}

		return moyenne;

	}

	/**
	 * Returns the promotion's best student
	 * 
	 * @return The promotion's best student
	 */
	public Etudiant getMajor() {

		Etudiant etud = null;

		double moyMax = this.moyenneMax();

		int i = 0;

		boolean trouve = false;

		while (i < listeEtudiants.length && !trouve) {

			if (listeEtudiants[i].moyenneGenerale() == moyMax) {

				etud = listeEtudiants[i];

				trouve = true;

			}

			i++;

		}

		return etud;

	}

	/**
	 * Returns the desired student based on his name
	 * 
	 * @param nom The student's name
	 * @return The desired student
	 */
	public Etudiant getEtudiant(String nom) {

		Etudiant etud = null;

		if (nom != null) {

			for (int i = 0; i < this.listeEtudiants.length; i++) {

				if (this.listeEtudiants[i].getNom().equals(nom)) {

					etud = this.listeEtudiants[i];

				}

			}

			if (etud == null) {

				System.out.println("Student isn't part of the promotion");

			}

		} else {

			System.out.println("The name can't be null");

		}

		return etud;

	}

	/**
	 * Returns a string containing the promotion's information
	 */
	public String toString() {

		String str = "\n";

		str += "Promotion's name : " + this.nom + "\n";

		str += "Number of students : " + this.listeEtudiants.length + "\n";

		str += "Average mark : " + this.moyenne() + "\n";

		return str;

	}

}
